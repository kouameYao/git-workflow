# Comment travailler, contribuer à projet de développeur avec git :star2:

## SOMMAIRE

- [Prérequis](#0️⃣-prérequis)
- [Le workflow avec git et gitlab dans vos différents projets.](#1️⃣-le-workflow-avec-git-et-gitlab-dans-vos-différents-projets)
- [Pusher vôtre code](#2️⃣-pusher-mon-code-sur-vôtre-dépôt-sur-origin)
- [Faire valider son travail](#3️⃣-faire-valider-son-travail)
- [Comment Faire valider son travail : cela passe par Pull Request](#4️⃣-comment-faire-valider-son-travail--cela-passe-par-pull-request-pr)
- [Contributeurs](#5️⃣-contributeurs)

### :zero: Prérequis :

---

- Etre à l’aise avec [Git](https://git-scm.com/)
- Connaitre [Gitlab](https://gitlab.com/) ou [Github](https://github.com/)

Grand merci à tous pour votre contribution. Ce document vous montre la manière de contribuer à un projet dans le context de l'entrepriese. Il vous servira de guide et vous donnera les bases. Ce n'est donc pas une Evangile.

### :one: Le workflow avec git et gitlab dans vos différents projets.

---

Si vous êtes déjà un habitué, ce document pourrait s'averer moins utile. Dans le cas contraire, vous êtes au bon endroit.

Lorsque l'on vous ajoute à une projet, c'est pour prendre part d'une manière quelconqu'on. La première étape est de recupérer ce projet(même si toutes les entreprises ne sont pas pour, moi je vous le recommende fortement).

Pour le faire, vous pouvez soit le `[fork]()` ou le `[clone]()`.

- **Méhode 1 : Cloner le projet.**

Pour le "cloner", copiez le lien https du projet auquel vous êtes ajouté (`comme le montre la figure suivante`).

![clone un projet!](/img/clone.png "Cliquez sur le bouton clone puis copiez le lien")

Dans un terminal sur votre ordinateur lancez la commande suivante

```bash
git clone "le lien que vous avez copiez"
```

Une fois que c'est terminé, rendez-vous à l'endroit ou le projet a été deposé. **TADAAAA !!!, vous avez desormais le projet en local, facile n'est pas 🤗**

- **Méhode 2 : Forker le projet.**

Pour forker le projet, cliquez sur le bouton `fork` [*avec l'icone en Y*] du projet en question (`comme le montre la figure suivante`)

![Forker un projet!](/img/forker-1.png "Cliquez sur le bouton avec l'icone en Y")

Vous verrez apparaitre le formulaire suivant. Remplissez-le puis cliquez sur `fork project` et le projet fera partie de vos projets désormais. Mais il sera lié au projet de base d'une certaine manière.

![Forker un projet!](/img/forker-2.png "Cliquez sur le bouton Fork project")

🔆 _Vous pouvez maintenant cloner le projet que vous avez devant vous, afin de l’avoir localement. En fait, ce projet vous appartient, et il est bien différente du projet de base. Même si l'on venait à vous supprimer sur le projet de base, le votre restera. Mais retenez une fois de plus qu'ils sont liés_

Comme dans la première méthode, faites

```bash
git clone "lien du vôtre dépot après l'avoir fork"
```

Dans les deux méthodes ci-dessus, si vous exécutez la commande

```bash
git remote -v
```

vous verrez le dépôt distant avec le nom origin (par defaut).
Aussi faut-il ajouter que le projet vient avec une branche par défaut: master ou main. Sauf si lors du clone ou du fort vous précisez la branche que vous souhaiterai prendre. Mais comment le faire dans ce cas ?... Attendez !!!, je ne vais pas tous vous livrer quand même, cherchez un peu.

Bon d'accord, je suis gentil aujourd'hui 😅, donc je vous donne comment faire dans le cas du clone.
Pour cloner une branche spécifique, faites

```bash
git clone -b <nom-de-la-branche-a-cloner> <lien-du-depot-distant>
```

🔆 _**-b** est une alias de **--branch**_.

### Tous ce qui suit appliqué après la mêthode 2.

### Et oui, ce n'est pas fini, vous devez lier maintenant vôtre dépôt local à celui que vous avez forker.

Pour le faire, entrez:

```bash
git remote add upstream "lien du dépôt de base : celui que vous avez forker"
```

🔆 _**upstream**_ est le nom que vous donnez au dépot distant en question

> _Pour Voir les dépots distants faites à nouveau faites_

```bash
git remote -v
```

🔆 **A retenir** : pour chaque fonctionnalité à implémenter, vous devez créer une nouvelle `branche`

Le projet vient avec une branche (master ou main par défaut) vous avais-je dis. Donc, à partir de cette branche, créez une nouvelle branche : `fonctionnalite1`. Ensuite déplacez-vous dans cette nouvelle branche. 👨‍💻Travaillez !!!

> _Créer une branche et y basculer_

```bash
git checkout -b fonctionnalite1
```

### :two: Pusher votre code sur vôtre dépôt: sur origin

---

Une fois que vous avez terminé ce que vous avez à faire, `pusher` vôtre travail sur vôtre dépôt distant. Et avant de pusher rassurez-vous de récupérer ce qui est sur la branche master ou main du projet de base. Cela vous évite les conflits inutile(Je parlerai de la gestion des conflits dans un article dédié).

J'ai besoin que vous soyez concentré. Vous allez mettre votre au brouillons, avant la récupération

```bash
git add .
git stash -m "WIP : suivi du message qui indique le travail que vous avez fait"
git checkout master/main
git pull --rebase upstream master/main
git branch -D fonctionnalite1
git checkout -b fonctionnalite1
git stash pop
git add .
```

> _Vérif et après_

```bash
git add .
git commit -m "bon message de commit bref mais decrivant le travail que vous voulez soumettre"
```

Et quand tout est bon, faites

```ts
git push -u origin `fonctionnalite1`
```

#### Important : vous devez encadrer chacun de vos push par deux pull rebase. Le premier avant de commencer à travailler et le deuxième avant de pousser son travail Nous verrons ceci un peu plus tard Voir

### :three: Faire valider son travail

---

Une autre chose à savoir est qu’ici (`En entreprise`), ce n'est pas à vous de valider vôtre propre travail. À moins que vous soyez un `Supérieur`(master, superviseur, manager, appelé le comme vous voulez. J'appelerai le mien "Master" - @Al-Hassan ). Et le processus de validation de vôtre travail passe par un `merge request (MR) ou un pull request dans le cas de github`. Ne vous inquietez pas quand vous entendrez parler de `PR`. Il s'agit en fait de `Pull Request`. C'est la même chose. Cela depend du fait que vous êtes sur `github PR` ou `gitlab MR`

### :four: Comment faire valider son travail : cela passe par Pull Request (`PR`) ?

---

Après un `push`, git vous fournit un lien qui ressemble à peu près à ce qui suit :
remote : https://gitlab.com/userName/projectName/-/merge_requests/new?merge_request%5Bsource_branch%5D=branchName

![Faire un PR!](/img/pr.png "Cliquez sur le lien")

Cliquez sur ce lien puis renseignez les informations demandées. Ces informations concernent `Assignee` (le nom d'utilisateur github/gitlab de celui à qui vous assignez le PR probablement vous-même), `Reviewer`(le nom d'utilisateur github/gitlab de vôtre Master)... `Milestone`, `Labels` peuvent rester par défaut. Une fois renseignées, cliquez sur le bouton `create merge request`. C'est fait ? Bravo !

### Attendez donc que vôtre Master valide vôtre travail ou qu'il le critique ou qu'il vous propose une amélioration.

### :warning: Sachez qu' un Master a beaucoup à faire. :warning:

En attendant qu’il valide vôtre travail, vous devez continuer de travailler. Créez donc une nouvelle branche `fonctionnalite2` depuis la branche `fonctionnalite1`. Si bien sur les deux fonctionnalités sont liées. Continuez de travailler _`git add . && git commit -m "message du commit")`_ comme d’habitude.

🔆 Lorsque vous allez vouloir faire valider vôtre travail, c’est la branche `fonctionnalite2` que vous devez push cette fois-ci... (_Cf :two: et :four:_)

🔆 Retenez que celle-ci est la plus à jour.

NB : lorsque la fonctionnalité à implémenter dans la branche `fonctionnaliten` dépend de la branche `fonctionnaliten+1` créez donc la nouvelle branche à partir de l’ancienne. Dans le cas contraire revenez dans la branche `master ou main` grâce à un checkout.

NB : continuez !!!, créez une nouvelle branche, travaillez, pusher puis faites un PR. Il peut arriver que vous ayez jusqu'à 8, 10 PR en attente. Ce n'est pas grave :smile:. Rappelez à chaque fois vôtre Master et continuer :computer:

Ceci n'étant pas exhaustive, le document est donc appelé à évoluer. J'espère tout de même qu'il vous aura appris les bases.

### 🤾‍♂️ A vous de jouer

---

Forker ce projet et exercez-vous :computer:. Si ça vous a plu, donnez-moi une étoile.

### :five: Contributeurs

---

| nom & prenoms | username      | Rôle      |
| ------------- | ------------- | --------- |
| KOUAME Yao    | @kouameYao    | Rédacteur |
| Soro Lazeni   | @nahagamasoro | Reviewer  |
